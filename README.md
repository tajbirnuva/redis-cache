# Redis as Cache Memory In Spring Boot Project

## Redis Work Flow
![](Additional%20File/Diagram/Redis%20Work%20Flow.png)

<br>

___

## Steps For Docker Redis Image

- To start Redis Stack server using the redis-stack-server image, run the following command in terminal
```
docker run -d --name redis-stack -p 6379:6379 -p 8001:8001 redis/redis-stack:latest
```
**_Hints : If `redis-stack-server` image not exist in docker then the command pull the image and then run the image_**
- We can connect the server in our local machine using redis-cli, for this run the following command in terminal
```
docker exec -it redis-stack redis-cli
```
**_Hints : to check connectivity use `ping` command which return `pong`_**

<br>

___

## Steps For Redis Caching

- Add Dependency for Redis. `NoSQL`- `spring-boot-starter-data-redis`
- Add `@EnableCaching` annotation to Main / Starter Class
- Configure `application.properties` file for Redis
- Inherit `Serializable` interface in Dto Class
- In Service method use your desired cache annotation such as
  - `@Cacheable`
  - `@CachePut`
  - `@CacheEvict`
  - `@Caching` 

<br>

___
<br>

> **Tips:**<br>
> In the `Additional File`, Postman's APIs have been added. In order to use the APIs, you must import them into your Postman Application.

> **Warning:**<br>
> Make sure your Redis server is running before using this application. Otherwise, an error related to the Redis server connection may occur