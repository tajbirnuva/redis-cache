package com.example.redis.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
public class PersonDto implements Serializable {
    private long id;
    private String name;
    private String age;
}
